package com.anji.plus.ajpushlibrary.util;

import android.content.Context;
import android.os.Build;

public class BrandUtil {
    private static volatile BrandUtil instance;
    private Context mContext;

    public BrandUtil(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * 单例模式 获取实例的方法
     */

    public static BrandUtil getInstance(Context context) {
        if (instance == null) {
            synchronized (BrandUtil.class) {
                instance = new BrandUtil(context);
            }
        }
        return instance;
    }

    //华为
    public boolean isHuawei() {
        if (Build.BRAND == null) {
            return false;
        } else {
            return Build.BRAND.toLowerCase().equals("huawei") || Build.BRAND.toLowerCase().equals("honor");
        }
    }

    //小米
    public boolean isXiaomi() {
        return Build.BRAND != null && Build.BRAND.toLowerCase().equals("xiaomi");
    }

    //oppo
    public static boolean isOPPO() {
        return Build.BRAND != null && Build.BRAND.toLowerCase().equals("oppo");
    }

    //vivo
    public static boolean isVIVO() {
        return Build.BRAND != null && Build.BRAND.toLowerCase().equals("vivo");
    }

    //魅族
    public boolean isMeizu() {
        return Build.BRAND != null && Build.BRAND.toLowerCase().equals("meizu");
    }


}
