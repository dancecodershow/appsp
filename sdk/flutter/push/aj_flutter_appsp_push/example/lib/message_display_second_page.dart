import 'package:flutter/material.dart';

class MessageDisplaySecondPage extends StatefulWidget {
  MessageDisplaySecondPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _MessageDisplaySecondPageState();
  }
}

class _MessageDisplaySecondPageState extends State<MessageDisplaySecondPage> {
  @override
  Widget build(BuildContext context) {
    print("MessageDisplaySecondPage build ");
    return Scaffold(
      body: Center(
          child: InkWell(
        child: Text('当前是演示页面'),
        onTap: () {},
      )),
      appBar: AppBar(
        title: Text('二级页面'),
      ),
    );
  }
}
