package com.anji.plus.ajpushlibrary.oppo;

import android.content.Context;

import com.anji.plus.ajpushlibrary.AppSpPushConstant;
import com.heytap.msp.push.HeytapPushManager;
import com.heytap.msp.push.callback.ICallBackResultService;

public class OppoPushUtil {
    private Context context;

    public OppoPushUtil(Context context) {
        this.context = context;
    }

    //判断是否手机平台是否支持PUSH
    public boolean isSupportPush() {
        return HeytapPushManager.isSupportPush();
    }

    //sdk初始化
    public void init() {
        HeytapPushManager.init(context, true);
    }

    //注册OPPO PUSH推送服务
    public void register(ICallBackResultService mPushCallback) {
        HeytapPushManager.register(context, AppSpPushConstant.oppoAppKey, AppSpPushConstant.oppoAppSecret, mPushCallback);//setPushCallback接口也可设置callback

    }

    //解注册OPPO PUSH推送服务
    public void unRegister() {
        HeytapPushManager.unRegister();

    }

    //获取注册OPPO PUSH推送服务的注册ID
    public String getRegisterID() {
        return HeytapPushManager.getRegisterID();

    }

    // 暂停接收OPPO PUSH服务推送的消息
    public void pausePush() {
        HeytapPushManager.pausePush();
    }

    // 恢复接收OPPO PUSH服务推送的消息，这时服务器会把暂停时期的推送消息重新推送过来
    public void resumePush() {
        HeytapPushManager.resumePush();
    }

    // 弹出通知栏权限弹窗（ColorOS 5.0以上有效）
    public void requestNotificationPermission() {
        HeytapPushManager.requestNotificationPermission();
    }

    //打开通知栏设置界面
    public void openNotificationSettings() {
        HeytapPushManager.openNotificationSettings();
    }

    // 获取通知栏状态，从callbackresultservice回调结果
    public void getNotificationStatus() {
        HeytapPushManager.getNotificationStatus();
    }

}


