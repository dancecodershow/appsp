package com.anji.plus.ajpushlibrary.service;

import android.content.Context;
import android.text.TextUtils;

import com.anji.plus.ajpushlibrary.AppSpPushConfig;
import com.anji.plus.ajpushlibrary.AppSpPushLog;
import com.anji.plus.ajpushlibrary.base.AppSpPushBaseRequest;
import com.anji.plus.ajpushlibrary.http.AppSpPushCallBack;
import com.anji.plus.ajpushlibrary.http.AppSpPushHttpClient;
import com.anji.plus.ajpushlibrary.http.AppSpPushPostData;

/**
 * Copyright © 2018 anji-plus
 * 安吉加加信息技术有限公司
 * http://www.anji-plus.com
 * All rights reserved.
 * <p>
 * 将厂商token和极光的regId上传给服务器
 * </p>
 */
public class AppSpPushServiceImpl extends AppSpPushBaseRequest implements IAppSpService {
    private AppSpPushHandler appSpHandler;

    public AppSpPushServiceImpl(Context mContext, String appKey, AppSpPushHandler appSpHandler) {
        super(mContext, appKey);
        this.appSpHandler = appSpHandler;
    }

    @Override
    public void putPushInfo() {
        if (TextUtils.isEmpty(appKey)) {
            AppSpPushLog.e("putPushInfo Appkey is null or empty");
            return;
        }
        AppSpPushPostData appSpPushPostData = getPostEncryptData();
        AppSpPushHttpClient client = new AppSpPushHttpClient();
//        client.request(AppSpRequestUrl.Host + AppSpRequestUrl.putPushInfo, appSpPostData, new AppSpCallBack() {
        client.request(AppSpPushConfig.requestUrl, appSpPushPostData, new AppSpPushCallBack() {
            @Override
            public void onSuccess(String data) {
                if (appSpHandler != null) {
                    appSpHandler.handleSuccess(data);
                }
            }

            @Override
            public void onError(String code, String msg) {
                if (appSpHandler != null) {
                    appSpHandler.handleExcption(code, msg);
                }
            }
        });

    }

}
