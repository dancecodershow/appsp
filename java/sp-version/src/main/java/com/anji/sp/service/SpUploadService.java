package com.anji.sp.service;

import com.anji.sp.model.ResponseModel;
import com.anji.sp.model.vo.SpUploadVO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 字典表
 *
 * @author Kean
 * @date 2020/06/23
 */
public interface SpUploadService {
    /**
     * 上传文件
     * @param spUploadVO
     * @return
     */
    ResponseModel uploadFile(SpUploadVO spUploadVO);

    /**
     * 图片上传
     * @param spUploadVO
     * @return
     */
    ResponseModel uploadPictureFile(SpUploadVO spUploadVO);

    /** 根据fileId显示图片或者下载文件
     * @param request
     * @param response
     * @param fileId
     * @return
     */
    ResponseEntity<byte[]> download(HttpServletRequest request, HttpServletResponse response, @PathVariable("fileId") String fileId);
}