package com.anji.sp.push.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 推送配置项
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PushMsgReceiveVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "配置id")
    private Long id;

    /**
     * 应用ID
     */
    @ApiModelProperty(value = "应用ID")
    private Long appId;

    /**
     * 应用唯一key
     */
    @ApiModelProperty(value = "应用唯一key")
    private String appKey;

    /**
     * 服务加密使用
     */
    @ApiModelProperty(value = "服务加密使用")
    private String secretKey;

    /**
     * 极光appkey
     */
    @ApiModelProperty(value = "极光appkey")
    private String jgAppKey;
    /**
     * 极光msgIds
     */
    private String jgMsgIds;


    /**
     * 极光masterSecret
     */
    @ApiModelProperty(value = "极光masterSecret")
    private String jgMasterSecret;

    /**
     * 华为appId
     */
    @ApiModelProperty(value = "华为appId")
    private String hwAppId;

    /**
     * 华为AppSecret
     */
    @ApiModelProperty(value = "华为AppSecret")
    private String hwAppSecret;

    /**
     * 华为推送标题（默认建议应用名）
     */
    @ApiModelProperty(value = "华为推送标题（默认建议应用名）")
    private String hwDefaultTitle;

    /**
     * oppo appId
     */
    @ApiModelProperty(value = "oppo appId ")
    private String opAppId;

    /**
     * oppo AppSecret
     */
    @ApiModelProperty(value = "oppo AppSecret ")
    private String opAppSecret;

    /**
     * oppo 推送标题（默认建议应用名）
     */
    @ApiModelProperty(value = "oppo 推送标题 ")
    private String opDefaultTitle;

    /**
     * oppo AppKey
     */
    @ApiModelProperty(value = "oppo AppKey ")
    private String opAppKey;

    /**
     * oppo Master Secret
     */
    @ApiModelProperty(value = "oppo Master Secret ")
    private String opMasterSecret;

    /**
     * vivo appId
     */
    @ApiModelProperty(value = "vivo appId ")
    private String voAppId;

    /**
     * vivo AppSecret
     */
    @ApiModelProperty(value = "vivo AppSecret  ")
    private String voAppSecret;

    /**
     * vivo 推送标题（默认建议应用名）
     */
    @ApiModelProperty(value = "vivo 推送标题（默认建议应用名） ")
    private String voDefaultTitle;

    /**
     * vivo  vivoTaskIds
     */
    private String vivoTaskIds;

    /**
     * vivo AppKey
     */
    @ApiModelProperty(value = "vivo AppKey")
    private String voAppKey;

    /**
     * 小米appId
     */
    @ApiModelProperty(value = "小米appId")
    private String xmAppId;

    /**
     * 小米AppSecret
     */
    @ApiModelProperty(value = "小米AppSecret")
    private String xmAppSecret;

    /**
     * 小米msgIds
     */
    private String xmMsgIds;

    /**
     * 小米推送标题（默认建议应用名）
     */
    @ApiModelProperty(value = "小米推送标题（默认建议应用名）")
    private String xmDefaultTitle;

    /**
     * 华为AppKey
     */
    @ApiModelProperty(value = "华为AppKey ")
    private String xmAppKey;

    /**
     * 包名
     */
    @ApiModelProperty(value = "包名")
    private String packageName;

    /**
     * 0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG
     */
    @ApiModelProperty(value = "0--已禁用 1--已启用")
    private Integer enableFlag;

    /**  是否开启厂商通道 默认 0 */
    @ApiModelProperty(value = "是否开启厂商通道 默认 0")
    private Integer channelEnable;

    /**  0--未删除 1--已删除 DIC_NAME=DEL_FLAG */
    @ApiModelProperty(value = "0--未删除 1--已删除")
    private Integer deleteFlag;


    /**
     * 创建者
     */
    private Long createBy;

    /**
     * 创建时间
     */
    private LocalDateTime createDate;

    /**
     * 更新者
     */
    private Long updateBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateDate;

    /**
     * 备注信息
     */
    private String remarks;
}
