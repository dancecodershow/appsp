package com.anji.sp.push.service;

import com.anji.sp.model.ResponseModel;
import com.anji.sp.push.model.MessageModel;
import com.anji.sp.push.model.po.PushHistoryPO;
import com.anji.sp.push.model.vo.PushHistoryVO;
import com.anji.sp.push.model.vo.RequestSendBean;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 推送历史表 服务类
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
public interface PushHistoryService extends IService<PushHistoryPO> {

    /** 创建
     * @param vo
     * @return
     */
    ResponseModel create(PushHistoryVO vo);

    /**
     * 保存到推送消息历史中
     *
     * @param requestSendBean
     * @param messageModel
     */
    ResponseModel savePushHistory(RequestSendBean requestSendBean, MessageModel messageModel, String msgId, String targetType);

        /** 根据id修改
         * @param vo
         * @return
         */
    ResponseModel updateById(PushHistoryVO vo);

    /** 根据id删除
     * @param vo
     * @return
     */
    ResponseModel deleteById(PushHistoryVO vo);

    /** 根据id查询一条记录
     * @param vo
     * @return
     */
    ResponseModel queryById(PushHistoryVO vo);

    /** 根据参数分页查询列表
     * @param vo
     * @return
     */
    ResponseModel queryByPage(PushHistoryVO vo);

    /** 根据appkey和msgId查询推送历史
     * @param vo
     * @return
     */
    ResponseModel queryByMsgId(PushHistoryVO vo, int i);
}
